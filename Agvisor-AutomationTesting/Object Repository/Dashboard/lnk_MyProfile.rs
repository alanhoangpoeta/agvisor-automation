<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lnk_MyProfile</name>
   <tag></tag>
   <elementGuidId>6dbe236f-49d3-441f-927d-078123c65ae0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[text()='My Profile']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[text()='My Profile']</value>
   </webElementProperties>
</WebElementEntity>
