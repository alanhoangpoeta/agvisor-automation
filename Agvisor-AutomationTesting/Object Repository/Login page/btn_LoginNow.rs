<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_LoginNow</name>
   <tag></tag>
   <elementGuidId>8bd7b255-7def-4f84-81cb-19a7e9bc4b55</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[text()[contains(.,'Login Now')]]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[text()[contains(.,'Login Now')]]</value>
   </webElementProperties>
</WebElementEntity>
