package customkeywords
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.By.ByXPath
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class commonkeywords {
	WebDriver driver = DriverFactory.getWebDriver()
	private List<WebElement> listCategory (){
		return driver.findElements(By.xpath("//div[@class='categories my-2']"))
	}
	private boolean selectCat (WebElement eCat){
		if (eCat != null){
			WebElement mCat = eCat.findElement(By.xpath(".//div[@class='flex-1 flex-auto']/div"))
			//Get Domain name
			String domainName = mCat.getText().trim()
			KeywordUtil.logInfo("Accessing: " + domainName)
			//Click Element
			mCat.click()
			WebUI.delay(2)
			KeywordUtil.markPassed("Clicked " + domainName + " successfully!")
			return true
		}else {
			KeywordUtil.markFailedAndStop("Element null")
			return false
		}
	}
	private WebElement softFailFindElement (WebElement eCat, String xpath){
		WebElement el = null
		KeywordUtil.logInfo("SoftFailFindElement!")
		try {
			el = eCat.findElement(By.xpath(xpath))
			KeywordUtil.logInfo("SoftFailFindElement - Try passed")
		}catch (Exception){
			el = null
			KeywordUtil.logInfo("SoftFailFindElement - Try failed")
		}
	}
	private int isLastCat (WebElement eCat){
		if (eCat != null){
			List<WebElement> arrow, pencil
			//Find arrow icon
			//arrow = softFailFindElement(eCat, ".//div[@class='p-3']")
			arrow = eCat.findElements(By.xpath(".//div[@class='p-3']"))
			//Find pencil icon
			//pencil = softFailFindElement(eCat, ".//div[@class='px-3 py-2']")
			pencil = eCat.findElements(By.xpath(".//div[@class='px-3 py-2']"))
			// Verify if found no arrow but pencil -  last element
			if (arrow.empty && !pencil.empty){
				KeywordUtil.logInfo("Last element!")
				return 1
			}else if (!arrow.empty){ //Verify if found arrow - not last element
				KeywordUtil.logInfo("Child element(s) found!")
				return 2
			}else { //arrow and pencil icon not found - error
				KeywordUtil.logInfo("Could not find Pencil icon and Child element")
				return 3
			}

		}else {
			KeywordUtil.markFailedAndStop("Element null")
			return 0
		}
	}
	private void randomSelect (String xpath){
		List<WebElement> lOptions = driver.findElements(By.xpath(xpath))
		if (lOptions != null){
			Random random = new Random()
			int index = random.nextInt(lOptions.size())
			lOptions.get(index).click()
			KeywordUtil.markPassed("Random select successfully")
		}else {
			KeywordUtil.markWarning("Random Select: Failed! List options not found")
		}
	}
	private void randomSlide (){
		//Find Slider
		WebElement eSlider = driver.findElement(By.xpath("//span[@class='range-slider-rail']"))
		if (eSlider != null){
			int x = eSlider.location.getX()
			Random random = new Random()
			int rdWidth = random.nextInt(eSlider.getSize().getWidth())
			Actions executer = new Actions(driver)
			executer.moveToElement(eSlider).click().dragAndDropBy(eSlider, x+rdWidth, 0).build().perform()
			KeywordUtil.markPassed("Random slide successfully")
		}else {
			KeywordUtil.markWarning("Random Slide: Failed! Slider not found")
		}
	}

	private void setEducationCat (WebElement eCat){
		if (eCat != null){
			WebElement pencil = eCat.findElement(By.xpath(".//div[@class='px-3 py-2']"))
			if (pencil != null){
				pencil.click()
				WebUI.delay(2)
				//Random select Levels
				randomSelect("//input[@name='levels']/following::label[1]")
				//Random slide
				randomSlide()
				//Random select affiliations
				randomSelect("//input[@name='affiliations']/following::label[1]")
				WebUI.click(findTestObject('Areas Of Expertise/btn_SaveEducation'))
				WebUI.delay(1)
				KeywordUtil.markPassed("Set Education successfully")
			}else {
				KeywordUtil.markError("Cannot find pencil icon")
			}
		}else {
			KeywordUtil.markFailedAndStop("Element null")
		}
	}
	private void listAndSelectRecursion (){
		List<WebElement> lE = listCategory()
		if (lE != null){
			WebElement[] eCat = new WebElement[lE.size()]
			for(int i = 0; i < lE.size(); i++){
				eCat[i] = lE[i]
				//Verify isLast element
				int isLast = isLastCat(eCat[i])
				if (isLast == 1){
					//Set Education
					setEducationCat(eCat[i])
				} else if (isLast == 2){
					//Moving forward
					if(selectCat(eCat[i])){ //If clicked domain successfully
						//Recursion
						listAndSelectRecursion()
					}
				}else {
					KeywordUtil.markWarning("Incorrect Element")
				}
			}
		}
		//Go Back
		WebUI.click(findTestObject('Object Repository/Areas Of Expertise/btn_Back'))
		WebUI.delay(2)
		KeywordUtil.logInfo("Listed and Selected Elements!")
	}

	@Keyword
	public void executeSetDataCat (){
		List<WebElement> lDomain = listCategory()
		if (lDomain == null){
			KeywordUtil.markFailed("List Category Null")
		}else {
			WebUI.comment(lDomain.size().toString())
			WebElement[] eDomain = new WebElement[lDomain.size()]
			//Loop for select all domains in the list
			for (int i=0; i < lDomain.size(); i++){
				eDomain[i] = lDomain[i]
				if(selectCat(eDomain[i])){ //If clicked domain successfully
					listAndSelectRecursion()
				}
			}
			WebUI.click(findTestObject('Object Repository/Areas Of Expertise/btn_SaveCat'))
			WebUI.delay(2)
			KeywordUtil.markPassed("Set Data Category successfully!")
		}
	}
}