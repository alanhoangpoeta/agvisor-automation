import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Login page/txt_EmailAddress'), GlobalVariable.username)

WebUI.setText(findTestObject('Login page/txt_Password'), GlobalVariable.password)

WebUI.click(findTestObject('Login page/btn_LoginNow'))

WebUI.waitForElementPresent(findTestObject('Dashboard/img_ProfileAvatar'), 15)

WebUI.delay(5)

WebUI.click(findTestObject('Dashboard/img_ProfileAvatar'))

WebUI.waitForElementClickable(findTestObject('Dashboard/lnk_MyProfile'), 5)

WebUI.click(findTestObject('Dashboard/lnk_MyProfile'))

WebUI.waitForElementClickable(findTestObject('AGvisor Profile/btn_InterestDomain'), 10)

WebUI.click(findTestObject('AGvisor Profile/btn_InterestDomain'))

WebUI.delay(5)

WebUI.waitForElementPresent(findTestObject('Areas Of Expertise/tit_AreasOfExpertise'), 30)

CustomKeywords.'customkeywords.commonkeywords.executeSetDataCat'()

WebUI.delay(5)

